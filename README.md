# backup data for rds
## 사용법
### local test
1. 연결할 서버와 ssh 터널링 하여 연결
2. config.py 에 설정
3. 실행

## build and deploy
### docker image 사용시
1. setup.sh 실행
```
# eu 인 경우
sudo docker build -t yumesaka/rds_backup_eu .
sudo docker push yumesaka/rds_backup_eu
```

2. ec2 접속 후 
``` 
sudo docker pull  yumesaka/rds_backup_eu
```

### 소스코드 업로드 하여 ec2에서 빌드시

1. ec2 접속 아래 명령 실행
```
mkdir project
cd project/
mkdir copy_data_postgres
mkdir rds_backup_logs
```
2. Forklift 를 통해서 copy_data_postgres 폴더 아래 소스코드 업로드
## container
### image 생성
```
cd copy_data_postgres
sudo docker build -t rds_backup .
```