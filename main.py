import os
import sys
import util.DBManager as DBM
from util import s3_util
import pandas as pd
import datetime
from dateutil import tz
from util.config import DEVICE_TYPE
import shutil
import logging.config
import json
import traceback

query_version = "select version();"
# q_get_backup_data_all_query_format_for_copy = "select * from tb_opr_mon_state_pms_gen2_hist " \
#                                               "where colec_dt" \
#                                               "between date(now()) - '{0} day'::interval - '1 hour'::interval and date(now()) - '{1} day'::interval " \
#                                               " order by device_id, colec_dt "

q_get_backup_data_all_query_format_for_copy = "select * from tb_opr_mon_state_{0}_hist " \
                                              "where colec_dt >= '{1}' and colec_dt < '{2}' " \
                                              " order by device_id, colec_dt "

# target_day = 91
# logger = None


def remove_all_file(file_path):
    if os.path.exists(file_path):
        shutil.rmtree(file_path)
        logger.debug('{0} directory delete'.format(file_path))


def make_directory(file_path):
    if not os.path.isdir(file_path):
        os.makedirs(file_path)

def set_logger():
    config = json.load(open('util/logger-config.json'))
    logging.config.dictConfig(config)
    global logger
    logger = logging.getLogger(__name__)

def execute_backup(date_of_backup_data,trunc_to_hour,start_time,device_type,db_manager):
    try:
        for i in range(24):
            query_st_time_str = (trunc_to_hour + datetime.timedelta(hours=i)).strftime(
                "%Y/%m/%d %H:%M:%S")
            query_end_time_str = (trunc_to_hour + datetime.timedelta(hours=i+1)).strftime(
                "%Y/%m/%d %H:%M:%S")


            # print(db_manager.execute_select(query_version))

            # 1.make file path
            # print(q_get_backup_data_all_query_format.format(target_day, target_day - 1))
            file_dir = './{0}/'.format('tmp')
            file_path_raw = file_dir + "raw_{0}/".format(device_type)
            s3_dir="opr_mon_state_{0}_1min".format(device_type)

            csv_file_name = 'opr_mon_state_{0}_1min_data_{1}.csv'.format(device_type,(trunc_to_hour + datetime.timedelta(hours=i)).strftime(
                "%Y-%m-%d_%H"))
            parquet_file_name = 'opr_mon_state_{0}_1min_data_{1}.snappy.parquet'.format(device_type,(trunc_to_hour + datetime.timedelta(hours=i)).strftime(
                "%Y-%m-%d_%H"))

            # 1.2 directory check and delete
            remove_all_file(file_dir)
            make_directory(file_path_raw)

            # 2.make csv file
            res=db_manager.copy_to_file(
                q_get_backup_data_all_query_format_for_copy.format(device_type,query_st_time_str, query_end_time_str),
                file_path_raw + csv_file_name)
            if res==0:
                continue

            logger.debug('make csv end time : {0}'.format(datetime.datetime.utcnow() - start_time))

            # 3.make parquet file
            # create_dt의 경우 pycopg의 자료형이 들어가게되어 parquet 파일 read error 발생 형변환 진행.


            df_backup_1min = pd.read_csv(file_path_raw + csv_file_name, dtype=DEVICE_TYPE.get(device_type))

            df_backup_1min['create_dt'] = pd.to_datetime(df_backup_1min['create_dt'], format='%Y-%m-%d %H:%M:%S',
                                                         errors='raise', utc=True)
            df_backup_1min['colec_dt'] = pd.to_datetime(df_backup_1min['colec_dt'], format='%Y-%m-%d %H:%M:%S',
                                                        errors='raise', utc=True)
            df_backup_1min.to_parquet(file_path_raw + parquet_file_name,
                                      engine='auto',
                                      compression='snappy',
                                      index=False
                                      ,coerce_timestamps='us')
            logger.debug('make parquet end time : {0} '.format(datetime.datetime.utcnow() - start_time))

            # 4.device_id 별로 파일 분리
            # list_device_id = df_backup_1min['device_id'].unique()
            # cnt = 0
            # for row in list_device_id:
            #     filter_list = df_backup_1min['device_id'] == row
            #     data = df_backup_1min.loc[filter_list, :]
            #     # print('{0} : split {1}file end time : {2}'.format(cnt + +, row, datetime.datetime.utcnow() - start_time))
            #     file_path_by_device = file_dir + row + "/"
            #     make_directory(file_path_by_device)
            #
            #     data.to_csv(file_path_by_device + csv_file_name, index=False)
            #     data.to_parquet(file_path_by_device + parquet_file_name,
            #                     engine='auto',
            #                     compression='snappy',
            #                     index=False)
            #     cnt = cnt + 1
            #     logger.debug(
            #         '{0} : split {1} file end time : {2}'.format(cnt, row, datetime.datetime.utcnow() - start_time))

            # 5.파일을 읽어 업로드
            cnt = 0
            for (path, dir, files) in os.walk(file_path_raw):
                for filename in files:
                    #  확장자만 분리
                    ext = os.path.splitext(filename)[-1]
                    # if ext == '.csv' or ext == '.parquet':
                    if ext == '.csv':
                        # print("%s %s" % (path, filename))
                        # print(path.replace('./tmp/', ''))
                        s3_util.upload_csv_file_to_s3(path + "/", filename,  date_of_backup_data,s3_dir)
                        cnt = cnt + 1
                    elif ext == '.parquet':
                        s3_util.upload_parquet_file_to_s3(path + "/", filename,  date_of_backup_data,s3_dir)
                        cnt = cnt + 1
    except Exception as e:
        logger.error(traceback.format_exc())


def main(target_day):
    # 0-0 logfile 설정
    set_logger()

    # 0-1 날짜 설정
    start_time = datetime.datetime.utcnow()
    date_of_backup_data = start_time - datetime.timedelta(days=target_day)
    # 0-2 쿼리에 넣을 날짜 문자열 생성
    trunc_to_hour=date_of_backup_data.replace(hour=0,minute=0, second=0, microsecond=0)
    db_manager = DBM.DBManager()
    # 백업 실행
    logger.debug('=====start backup : target_day = {0} '.format(target_day))
    for device_type in DEVICE_TYPE:
        execute_backup(date_of_backup_data, trunc_to_hour, start_time, device_type,db_manager)
    logger.debug('end time : {0}'.format(datetime.datetime.utcnow() - start_time))
    logger.debug('=============end============')




if __name__ == '__main__':
    if len(sys.argv) > 1:
        target_day = int(sys.argv[1])
    else:
        target_day = 91
    main(target_day)
