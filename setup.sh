#aws login
aws ecr get-login-password --region ap-northeast-2 | docker login --username AWS --password-stdin 688554574862.dkr.ecr.ap-northeast-2.amazonaws.com

#build image M1 (arm64 -> amd64)
docker buildx build --platform linux/amd64 -t 688554574862.dkr.ecr.ap-northeast-2.amazonaws.com/rds_backup_s3:rds_backup_s3_eu . --push

#build image Intel
#docker build-t 688554574862.dkr.ecr.ap-northeast-2.amazonaws.com/data-migration:import_s3_to_tsdb . --push