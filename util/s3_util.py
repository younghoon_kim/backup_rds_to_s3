import boto3
from util.config import BUCKET_NAME


def upload_file_to_s3(file_path, file_name, device_id, data_datetime):
    s3 = boto3.client('s3')

    with open(file_path + file_name, "rb") as f:
        s3.upload_fileobj(f, BUCKET_NAME, "backupdata-in-db/{0}/1min/{1}/{2}/{3}"
                          .format(device_id, data_datetime.year, data_datetime.month, file_name))


def upload_csv_file_to_s3(file_path, file_name, data_datetime, dir):
    s3 = boto3.client('s3')

    with open(file_path + file_name, "rb") as f:
        print(BUCKET_NAME + "backupdata-in-db/csv/{0}/year={1}/month={2}/day={3}/{4}"
                          .format(dir,data_datetime.year, data_datetime.month, data_datetime.day, file_name))
        s3.upload_fileobj(f, BUCKET_NAME, "backupdata-in-db/csv/{0}/year={1}/month={2}/day={3}/{4}"
                          .format(dir,data_datetime.year, str(data_datetime.month).zfill(2), str(data_datetime.day).zfill(2), file_name))


def upload_parquet_file_to_s3(file_path, file_name, data_datetime,dir):
    s3 = boto3.client('s3')

    with open(file_path + file_name, "rb") as f:
        print(BUCKET_NAME + "backupdata-in-db/parquet/parquet/{0}/year={1}/month={2}/day={3}/{4}"
              .format(dir,data_datetime.year, data_datetime.month, data_datetime.day, file_name))
        s3.upload_fileobj(f, BUCKET_NAME, "backupdata-in-db/parquet/{0}/year={1}/month={2}/day={3}/{4}"
                          .format(dir,data_datetime.year,  str(data_datetime.month).zfill(2), str(data_datetime.day).zfill(2), file_name))
