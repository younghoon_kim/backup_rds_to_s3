# aws dev-new
# HOST = 'dev-server-new.cbnazfqnk3mm.eu-central-1.rds.amazonaws.com'
# PORT = 5431
# DBNAME = 'wemsdb'
# USER = 'wems'
# PWD = 'qcells!'
#PATH = '-c search_path=wems'
# BUCKET_NAME = 'qhome-test'

# aws au
# HOST ='au-production-tsdb.qommand.qcells.com'
# PORT = 5432
# DBNAME = 'wemsdb'
# USER = 'wems_data'
# PWD = 'qcell!data'
#PATH = '-c search_path=wems'
# BUCKET_NAME = 'qhome-db-backup-au'

# aws eu
HOST ='eu-production-tsdb.qommand.qcells.com'
PORT = 5432
DBNAME = 'wemsdb'
USER = 'wems'
PWD = 'qcells!'
PATH = '-c search_path=wems'
BUCKET_NAME = 'qhome-db-backup-eu'

# aws eu
#HOST ='production-server.cbnazfqnk3mm.eu-central-1.rds.amazonaws.com'
#PORT = 5432
#DBNAME = 'wemsdb'
# USER = 'wems'
# PWD = 'qcells!'
#PATH = '-c search_path=wems'
#BUCKET_NAME = 'qhome-db-backup-eu'


# aws eu
# HOST ='production-server.cbnazfqnk3mm.eu-central-1.rds.amazonaws.com'
# PORT = 5432
# DBNAME = 'wemsdb'
# USER = 'wems'
# PWD = 'qcells!'
# BUCKET_NAME = 'qhome-db-backup-eu'

# docker timescaleDB
# HOST ='localhost'
# PORT = 5435
# DBNAME = 'query_speed_test'
# USER = 'postgres'
# PWD = 'password'

#터널링 후 테스트
#HOST= 'host.docker.internal'
# HOST ='localhost'
# PORT = 8585
# DBNAME = 'wemsdb'
# USER = 'wems'
# PWD = 'qcells!'
#PATH = '-c search_path=wems'

# BUCKET_NAME = 'qhome-db-backup-eu'

#aws seoul tsdb
# HOST = 'dev-server-new-tsdb.qcells-qonnect.com'
# PORT = 5432
# DBNAME = 'wemsdb'
# USER = 'wems'
# PWD = 'qcells!'
# PATH = '-c search_path=wems'
# BUCKET_NAME = 'qhome-db-backup-seoultsdb-test'

#aws eu - dev db
#HOST = 'dev-server-new-tsdb.qcells-qonnect.com'
#PORT = 5432
#DBNAME = 'wemsdb'
#SER = 'wems'
#PWD = 'qcells!'
#PATH = '-c search_path=wems'
#BUCKET_NAME = 'qhome-db-backup-eu'

#aws eu - stage db tunneling
# HOST = 'localhost'
# PORT = 8585
# DBNAME = 'wemsdb'
# USER = 'wems'
# PWD = 'qcells!'
#PATH = '-c search_path=wems'
# BUCKET_NAME = 'qhome-db-backup-eu'

#aws seoul [STAGING] tsdb
# HOST = 'seoul-staging-db.qcells-qonnect.com'
# PORT = 5432
# DBNAME = 'wemsdb'
# USER = 'wems'
# PWD = 'qcells!'
#PATH = '-c search_path=wems'
# BUCKET_NAME = 'qhome-db-backup-seoultsdb-staging'


DTYPE_GEN3 = {
'device_id': str,'oper_stus_cd': str,'energy_policy': str,
'pcs_ctrl_cmd1': str,'pcs_opmode1': str,'error_codes': str,
'dynamic_opt_flag' : float,'load_pw' : float,'load_pwh_day' : float,'load_pwh_day_delta' : float,
'core_pw' : float,
'core_pwh_day' : float,
'core_pwh_day_delta' : float,
'grid_pur_pwh_day' : float,
'grid_pur_pwh_day_delta' : float,
'grid_fdin_pwh_day' : float,
'grid_fdin_pwh_day_delta' : float,
'grid_st_cd' : float,
'smtr_st_cd' : float,
'meter_v' : float,
'meter_i' : float,
'meter_freq' : float,
'meter_active_pw' : float,
'meter_reactive_pw' : float,
'meter_pwfactor' : float,
'pcs_ctrl_cmd2' : float,
'pv_pwh_day' : float,
'pv_pwh_day_delta' : float,
'bt_chrg_pwh_day' : float,
'bt_chrg_pwh_day_delta' : float,
'bt_dchrg_pwh_day' : float,
'bt_dchrg_pwh_day_delta' : float,
'pcs_fd_pwh_day' : float,
'pcs_fd_pwh_day_delta' : float,
'pcs_pch_pwh_day' : float,
'pcs_pch_pwh_day_delta' : float,
'pcs_st_cd' : float,
'pcs_opmode2' : float,
'pv_avail_flag' : float,
'pv_stus_cd' : float,
'pv_pw' : float,
'pv_v1' : float,
'pv_i1' : float,
'pv_pw1' : float,
'pv_v2' : float,
'pv_i2' : float,
'pv_pw2' : float,
'pv_v3' : float,
'pv_i3' : float,
'pv_pw3' : float,
'bt_chrg_avail_flag' : float,
'bt_dchrg_avail_flag' : float,
'bt_stus_cd' : float,
'bt_pw' : float,
'bt_v' : float,
'bt_i' : float,
'inverter_v' : float,
'inverter_i' : float,
'inverter_pw' : float,
'inverter_freq' : float,
'dc_link_v' : float,
'pcs_grid_v' : float,
'pcs_grid_i' : float,
'pcs_grid_active_pw' : float,
'pcs_grid_reactive_pw' : float,
'pcs_grid_pwfactor' : float,
'pcs_grid_freq' : float,
'inverter_tgt_pw' : float,
'battery_tgt_pw' : float,
'pv_limit_pw' : float,
'pw_ctrl_pnt' : float,
'rack_cnt' : float,
'real_soc_avg' : float,
'user_soc_avg' : float,
'rack_id1' : float,
'rack_v1' : float,
'rack_i1' : float,
'soc1' : float,
'soh1' : float,
'cell_max_v1' : float,
'cell_max_v_module_id1' : float,
'cell_max_v_cell_id1' : float,
'cell_min_v1' : float,
'cell_min_v_module_id1' : float,
'cell_min_v_cell_id1' : float,
'cell_avg_v1' : float,
'cell_max_t1' : float,
'cell_max_t_module_id1' : float,
'cell_max_t_cell_id1' : float,
'cell_min_t1' : float,
'cell_min_t_module_id1' : float,
'cell_min_t_cell_id1' : float,
'cell_avg_t1' : float,
'rack_id2' : float,
'rack_v2' : float,
'rack_i2' : float,
'soc2' : float,
'soh2' : float,
'cell_max_v2' : float,
'cell_max_v_module_id2' : float,
'cell_max_v_cell_id2' : float,
'cell_min_v2' : float,
'cell_min_v_module_id2' : float,
'cell_min_v_cell_id2' : float,
'cell_avg_v2' : float,
'cell_max_t2' : float,
'cell_max_t_module_id2' : float,
'cell_max_t_cell_id2' : float,
'cell_min_t2' : float,
'cell_min_t_module_id2' : float,
'cell_min_t_cell_id2' : float,
'cell_avg_t2' : float,
'rack_id3' : float,
'rack_v3' : float,
'rack_i3' : float,
'soc3' : float,
'soh3' : float,
'cell_max_v3' : float,
'cell_max_v_module_id3' : float,
'cell_max_v_cell_id3' : float,
'cell_min_v3' : float,
'cell_min_v_module_id3' : float,
'cell_min_v_cell_id3' : float,
'cell_avg_v3' : float,
'cell_max_t3' : float,
'cell_max_t_module_id3' : float,
'cell_max_t_cell_id3' : float,
'cell_min_t3' : float,
'cell_min_t_module_id3' : float,
'cell_min_t_cell_id3' : float,
'cell_avg_t3' : float,


}
DTYPE_GEN2 = {
    'device_id': str,
    'oper_stus_cd': str,
    'pv_stus_cd': str,
    'bt_stus_cd': str,
    'grid_stus_cd': str,
    'ems_opmode': str,
    'pcs_opmode1': str,
    'pcs_opmode2': str,
    'feed_in_limit': str,
    'max_inverter_pw_cd': str,
    'pwr_range_cd_ess_grid': str,
    'pwr_range_cd_grid_ess': str,
    'pwr_range_cd_grid_load': str,
    'pwr_range_cd_ess_load': str,
    'pwr_range_cd_pv_ess': str,
    'pcs_flag0': str,
    'pcs_flag1': str,
    'pcs_flag2': str,
    'pcs_opmode_cmd1': str,
    'pcs_opmode_cmd2': str,
    'bms_flag0': str,
    'bms_diag0': str,
    'smtr_tp_cd': str,
    'smtr_modl_cd': str,
    'instl_rgn': str,
    'basicmode_cd': str,
    'sys_st_cd': str,
    'bt_chrg_st_cd': str,
    'bt_dchrg_st_cd': str,
    'pv_st_cd': str,
    'grid_st_cd': str,
    'smtr_st_cd': str,
    'dn_enable': str,
    'dc_start': str,
    'dc_end': str,
    'nc_start': str,
    'nc_end': str,
    'grid_code0': str,
    'grid_code1': str,
    'grid_code2': str,
    'pcon_bat_targetpower': str,
    'grid_code3': str,
    'bdc_module_code0': str,
    'bdc_module_code1': str,
    'bdc_module_code2': str,
    'bdc_module_code3': str,
    'fault5_realtime_year': str,
    'fault5_realtime_month': str,
    'fault5_day': str,
    'fault5_hour': str,
    'fault5_minute': str,
    'fault5_second': str,
    'fault5_datahigh': str,
    'fault5_datalow': str,
    'fault6_realtime_year': str,
    'fault6_realtime_month': str,
    'fault6_realtime_day': str,
    'fault6_realtime_hour': str,
    'fault6_realtime_minute': str,
    'fault6_realtime_second': str,
    'fault6_datahigh': str,
    'fault6_datalow': str,
    'fault7_realtime_year': str,
    'fault7_realtime_month': str,
    'fault7_realtime_day': str,
    'fault7_realtime_hour': str,
    'fault7_realtime_minute': str,
    'fault7_realtime_second': str,
    'fault7_datahigh': str,
    'fault7_datalow': str,
    'fault8_realtime_year': str,
    'fault8_realtime_month': str,
    'fault8_realtime_day': str,
    'fault8_realtime_hour': str,
    'fault8_realtime_minute': str,
    'fault8_realtime_second': str,
    'fault8_datahigh': str,
    'fault8_datalow': str,
    'fault9_realtime_year': str,
    'fault9_realtime_month': str,
    'fault9_realtime_day': str,
    'fault9_realtime_hour': str,
    'fault9_realtime_minute': str,
    'fault9_realtime_second': str,
    'fault9_datahigh': str,
    'fault9_datalow': str,
'pv_pw' : float,
'pv_pw_h' : float,
'pv_pw_co2' : float,
'cons_pw' : float,
'cons_pw_h' : float,
'cons_pw_co2' : float,
'bt_pw' : float,
'bt_chrg_pw_h' : float,
'bt_chrg_pw_co2' : float,
'bt_dchrg_pw_h' : float,
'bt_dchrg_pw_co2' : float,
'bt_soc' : float,
'bt_soh' : float,
'grid_pw' : float,
'grid_ob_pw_h' : float,
'grid_ob_pw_co2' : float,
'grid_tr_pw_h' : float,
'grid_tr_pw_co2' : float,
'pcs_pw' : float,
'pcs_fd_pw_h' : float,
'pcs_pch_pw_h' : float,
'pcs_tgt_pw' : float,
'rfsh_prd' : float,
'outlet_pw' : float,
'outlet_pw_h' : float,
'outlet_pw_co2' : float,
'pcs_comm_err_rate' : float,
'smtr_comm_err_rate' : float,
'm2m_comm_err_rate' : float,
'pv_v1' : float,
'pv_i1' : float,
'pv_pw1' : float,
'pv_v2' : float,
'pv_i2' : float,
'pv_pw2' : float,
'inverter_v' : float,
'inverter_i' : float,
'inverter_pw' : float,
'dc_link_v' : float,
'g_rly_cnt' : float,
'bat_rly_cnt' : float,
'grid_to_grid_rly_cnt' : float,
'bat_pchrg_rly_cnt' : float,
'rack_v' : float,
'rack_i' : float,
'cell_max_v' : float,
'cell_min_v' : float,
'cell_avg_v' : float,
'cell_max_t' : float,
'cell_min_t' : float,
'cell_avg_t' : float,
'tray_cnt' : float,
'smtr_pulse_cnt' : float,
'pv_max_pwr1' : float,
'pv_max_pwr2' : float,
'mmtr_slv_addr' : float,
'smtr_tr_cnter' : float,
'smtr_ob_cnter' : float,
'bt_real_soc' : float,
'load_main_pw' : float,
'load_sub_pw' : float,
'load_main_pw_h' : float,
'load_sub_pw_h' : float,
'inverter_freq' : float,
'bt_v' : float,
'bt_i' : float,
'meter_v' : float,
'meter_i' : float,
'meter_active_pw' : float,
'meter_reactive_pw' : float,
'meter_pwfactor' : float,
'meter_freq' : float,

}
DEVICE_TYPE = {"pms_gen2":DTYPE_GEN2,"pms_gen3":DTYPE_GEN3}

batch_num = 10